import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  updateCartItemAmount,
  updateCartItemSize,
} from "../../features/customerSlice";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { useNavigate } from "react-router-dom";
import Auth from "../../components/auth/auth";
import DeletionPopup from "../../components/deletionPopup/DeletionPopup";
import "./Cart.css";

const Cart = () => {
  const navigate = useNavigate();
  const cart = useSelector((state) => state.cart);
  const guest = useSelector((state) => state.guest);
  const isLoggedIn = useSelector((state) => state.customer !== null);
  const [showAuthOverlay, setShowAuthOverlay] = useState(false);
  const [products, setProducts] = useState([]);
  const dispatch = useDispatch();

  const [deletingItemIndex, setDeletingItemIndex] = useState(null);
  const [deletePopupActive, setDeletePopupActive] = useState(false);

  const getProducts = async () => {
    try {
      const response = await fetch(`${import.meta.env.VITE_API}products`);
      if (response.ok) {
        const data = await response.json();
        setProducts(data);
      } else {
        console.error("Failed to fetch products:", response.statusText);
      }
    } catch (error) {
      console.error("Error during product fetch:", error.message);
    }
  };

  useEffect(() => {
    getProducts();
  }, []);

  const displayAuthOverlay = () => {
    setShowAuthOverlay(true);
  };

  const closeAuthOverlay = () => {
    setShowAuthOverlay(false);
  };

  const getProductPrice = (productName, pizzaSize) => {
    const size = typeof pizzaSize === "string" ? pizzaSize.toLowerCase() : "";
    const product = products.find((p) => p.name === productName);
    if (!product) {
      return 0;
    }

    let sizeAdjustment = 0;

    switch (size) {
      case "medium":
        sizeAdjustment = 1;
        break;
      case "large":
        sizeAdjustment = 2;
        break;
      case "jumbo":
        sizeAdjustment = 3;
        break;
      default:
        sizeAdjustment = 0;
    }

    return product.price + sizeAdjustment;
  };

  const handleAmountChange = (index, newAmount) => {
    if (newAmount === 0 && !deletePopupActive) {
      setDeletingItemIndex(index);
      setDeletePopupActive(true);
      return;
    }
    const item = cart[index];
    if (item) {
      dispatch(
        updateCartItemAmount({
          _id: item._id,
          name: item.name,
          size: item.size,
          amount: newAmount,
        })
      );
    }
    setDeletingItemIndex(null);
    setDeletePopupActive(false);
  };

  const handleSizeChange = (index, newSize) => {
    const item = cart[index];
    if (item) {
      dispatch(
        updateCartItemSize({
          _id: item._id,
          name: item.name,
          size: newSize,
          amount: item.amount,
        })
      );
    }
  };

  const openCheckout = () => {
    console.log("guest: ", guest);
    console.log("isLoggedIn: ", isLoggedIn);

    if (isLoggedIn || guest) {
      navigate("/checkout");
    } else {
      displayAuthOverlay();
    }
  };

  return (
    <div className="cart">
    <div className="cart-content">
      <h2>Shopping Cart</h2>
      {cart.length > 0 ? (
        cart.map((item, index) => (
          <div key={index}>
            <p>
              <strong>Name:</strong> {item.name}
            </p>
            <strong>Size: </strong>
            {products && products.length > 0 && (
              <div className="dropdown">
                <button
                  className="dropbtn"
                  onClick={() => handleSizeChange(index, item.size)}
                >
                  {item.size}
                </button>
                <div className="dropdown-content">
                  {products
                    .find((p) => p.name === item.name)
                    ?.size?.map((size, sizeIndex) => (
                      <a
                        key={sizeIndex}
                        href="#"
                        onClick={() => handleSizeChange(index, size)}
                      >
                        {size}
                      </a>
                    ))}
                </div>
              </div>
            )}
            <p>
              <strong>Amount:</strong>
              <input
                type="number"
                value={item.amount}
                onChange={(e) =>
                  handleAmountChange(index, parseInt(e.target.value, 10))
                }
                min="0"
              />
            </p>
            <p>
              <strong>Price:</strong> €
              {(getProductPrice(item.name, item.size) * item.amount).toFixed(2)}
            </p>
            <FontAwesomeIcon
              icon={faTrash}
              onClick={() => handleAmountChange(index, 0)}
            />
            <hr />
            {deletingItemIndex === index && (
              <DeletionPopup
                onDelete={() => handleAmountChange(index, 0)}
                onCancel={() => {
                  setDeletingItemIndex(null);
                  setDeletePopupActive(false);
                }}
              />
            )}
          </div>
        ))
      ) : (
        <p>Cart is empty.</p>
      )}
      {cart.length > 0 && <button onClick={openCheckout}>CHECKOUT</button>}
      {showAuthOverlay && <Auth onClose={closeAuthOverlay} />}
    </div>
    </div>
  );
};

export default Cart;
