import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import CardInput from "../../components/cardInput/CardInput.jsx";
import CustomerDataInput from "../../components/customerDataInput/CustomerDataInput";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import "./Checkout.css";
import { clearCart } from "../../features/customerSlice";

const Checkout = () => {
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.cart);
  const isLoggedIn = useSelector((state) => state.customer !== null);
  const customer = useSelector((state) => state.customer);
  const token = useSelector((state) => state.token);
  //   const [customerDataValid, setCustomerDataValid] = useState(false);
  //   const [cardDataValid, setCardDataValid] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    if (cart.length === 0) {
      navigate("/");
    }
  }, []);

  const handleOrderButtonClick = async () => {
    //if (customerDataValid && cardDataValid) {
    try {
      const response = await fetch(`${import.meta.env.VITE_API}checkout`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        //body: JSON.stringify("values"),
      });

      if (response.ok) {
        const data = await response.json();
        if (isLoggedIn) {
          addOrderToHistory(data.orderId);
        }
        else {
          dispatch(clearCart());
        }

        navigate(`/tracking/${data.orderId}`);
      }
    } catch (error) {
      console.error("Error", error.message);
    }
    //}
  };

  const addOrderToHistory = async (orderId) => {
    try {
      const response = await fetch(
        `${import.meta.env.VITE_API}orderhistory/${orderId}`,
        {
          method: "POST",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
            "x-api-key": import.meta.env.VITE_API_KEY,
          },
          body: JSON.stringify({ customerId: customer.id, items: cart }),
        }
      );
      if (response.ok) {
        const success = await response.json();
        dispatch(clearCart());
        console.log("Success:", success);
      }
    } catch (error) {
      console.error("Error saving cart to server:", error.message);
    }
  };

  return (
    <div className="checkout">
      <div className="delivery-info">
        <div className="left">
          <h2>Shipping Information</h2>
          <CustomerDataInput />
        </div>
        <div className="right">
          <h2>Payment Method</h2>
          <CardInput />
        </div>
        <button className="order-btn" onClick={handleOrderButtonClick}>
          ORDER
        </button>
      </div>
      
    </div>
  );
};

export default Checkout;
