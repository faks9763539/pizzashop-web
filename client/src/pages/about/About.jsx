import { forwardRef } from "react";
import "./About.css";
import image from "../../assets/chef.png";

const About = forwardRef((props, ref) => {
  return (
    <div className="about" ref={ref}>
      <img src={image} alt="" className="chef" />
      <div className="aboutUs">
        <h3 className="about-title">About Us</h3>
        <p className="about-text">Welcome to LT Pizza, where passion meets perfection in every slice! Established in 2010, LT Pizza is a family-owned pizzeria nestled in the heart of Zagreb. What started as a humble endeavor driven by a love for authentic flavors and a commitment to quality has blossomed into a local favorite.</p>
        <p className="about-text">At LT Pizza, we take pride in crafting mouthwatering pizzas that are a true testament to the artistry of our chefs. Our journey began with a simple goal - to create a haven for pizza enthusiasts seeking a delightful escape from the ordinary. With a focus on using only the finest, freshest ingredients, each pizza is a symphony of flavors, bringing together the best of traditional and innovative toppings.</p>
      </div>
    </div>
  );
});

export default About;
