import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import "./Tracking.css";

const orderStatuses = [
  "Order confirmed",
  "Order being prepared",
  "Order ready",
  "Order out for delivery",
  "Order delivered",
];

const Tracking = () => {
  const { orderId } = useParams();
  const [statusMessage, setStatusMessage] = useState("");

  useEffect(() => {
    getTrackingInfo();
    const intervalId = setInterval(() => {
      getTrackingInfo();
    }, 2000);
    return () => clearInterval(intervalId);
  }, []);

  const getTrackingInfo = async () => {
    try {
      const response = await fetch(
        `${import.meta.env.VITE_API}orderstatus/${orderId}`
      );
      if (response.ok) {
        const data = await response.json();
        console.log(data.statusMessage);
        setStatusMessage(data.statusMessage);
      } else {
        console.error("Failed to fetch products:", response.statusText);
      }
    } catch (error) {
      console.error("Error during product fetch:", error.message);
    }
  };

  return (
    <div className="tracking">
      <div>
      <h1>
        Your order is confirmed!
      </h1>
      <p className="tracking-order-id">Order ID: {orderId}</p>
      </div>
      <div className="statuses">
        {orderStatuses.map((status, index) => (
          <div
            key={index}
            className={
              status === statusMessage ? "status active-status" : "status"
            }
          >
            <p>{status}</p>
            <span
              className={status === statusMessage ? "dot active-status" : "dot"}
            ></span>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Tracking;
