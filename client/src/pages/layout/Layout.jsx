import "./Layout.css";
import Home from "../home/Home";
import SpecialOffer from "../specialOffer/SpecialOffer";
import About from "../about/About";
import Contact from "../contact/Contact";
const Layout = ({ homeRef, specialOfferRef, aboutRef, contactRef }) => {
  return (
    <div className="layout">
      <Home ref={homeRef} />
      <SpecialOffer ref={specialOfferRef} />
      <About ref={aboutRef} />
      <Contact ref={contactRef} />
    </div>
  );
};

export default Layout;
