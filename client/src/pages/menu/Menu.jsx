import React, { useEffect, useState, forwardRef } from "react";
import Product from "../../components/product/Product";
import "./Menu.css";

const Menu = () => {
  const [products, setProducts] = useState([]);

  const getProducts = async () => {
    try {
      const response = await fetch(`${import.meta.env.VITE_API}products`);
      if (response.ok) {
        const data = await response.json();
        setProducts(data);
      } else {
        console.error("Failed to fetch products:", response.statusText);
      }
    } catch (error) {
      console.error("Error during product fetch:", error.message);
    }
  };

  useEffect(() => {
    getProducts();
  }, []);

  return (
    <div className="menu">
      <h1 className="menu-title">Menu</h1>
      <div className="product-list">
        {products.map((product) => (
          <Product key={product._id} product={product} />
        ))}
      </div>
    </div>
  );
};

export default Menu;
