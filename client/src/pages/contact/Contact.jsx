import { forwardRef } from "react";
import "./Contact.css";
import image from "../../assets/contact-pizza-pic.png";

const Contact = forwardRef((props, ref) => {
  return (
    <div className="contact" ref={ref}>
      <div className="contact-info">
        <h1>Contact us!</h1>
        <p>We'd love to hear from you! Whether you have a question, feedback, or just want to say hello, there are several ways to get in touch with us.</p>
        <div className="row01">
          <div> 
            <h2 className="contact-titles">Visit us:</h2>
            <p>LT Pizza</p>
            <p>Ulica 1</p>
            <p>10000, Zagreb</p>
          </div> 
          <div> 
            <h2 className="contact-titles">Bussines hours:</h2>
            <p>Monday - Saturday: 11:00 AM - 9:00 PM</p>
            <p>Sunday: 12:00 PM - 6:00 PM</p>
          </div> 
        </div>
        <div className="row02">
          <div>
            <h2 className="contact-titles">Call us:</h2>
            <p>Phone: +385 91 1234 567</p>
          </div>
          <div className="row2column2">
            <h2 className="contact-titles">Email us:</h2>
            <p>Email: info@ltpizza.com</p>
          </div>
        </div>
        <h2 className="contact-titles catering">Catering and events</h2>
        <p>Planning a special event? Let LT Pizza cater to your cravings! Contact our events team at events@ltpizza.com to discuss catering options and personalized menus.</p>
        <h2 className="contact-titles">Feedback</h2>
        <p>Your feedback is invaluable to us! Share your thoughts, suggestions, or experiences by emailing feedback@ltpizza.com. We appreciate your input as we strive to make every visit to LT Pizza exceptional.</p>
        <p>Thank you for choosing LT Pizza. We look forward to serving you and creating memorable moments together!</p>
      </div>
      <img src={image} alt="" className="contact-photo"/>
    </div>
  );
});

export default Contact;
