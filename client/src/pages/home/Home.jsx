import React, {forwardRef } from "react";
import "./Home.css";
import image from "../../assets/homePage_pizza.png";

const Home = forwardRef((props, ref) => {
  return (
    <div className="home" ref={ref}>
      <div className="title-whole">
        <h2>It’s not just a pizza,</h2>
        <div className="title-pt2">
          <h2>it’s an</h2>
          <h2 className="experience">experience</h2>
        </div>
      </div>

      <img src={image} alt="" className="home-img"/>
      
    </div>
  );
});

export default Home;
