import { useParams } from "react-router-dom";
import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import CustomerDataInput from "../../components/customerDataInput/CustomerDataInput";
import CardInput from "../../components/cardInput/CardInput.jsx";
import ConfirmLoginDataPopup from "../../components/confirmDataPopup/ConfirmLoginDataPopup.jsx";

import "./Profile.css";

const Profile = () => {
  const [showLoginPopup, setShowLoginPopup] = useState(false);
  const [customerData, setCustomerData] = useState({});
  const customer = useSelector((state) => state.customer);
  const isLoggedIn = useSelector((state) => state.customer !== null);
  const token = useSelector((state) => state.token);

  const handleSaveButtonClicked = () => {
    setShowLoginPopup(true);
  };

  const saveCustomerDataToServer = async (values) => {
    if (isLoggedIn) {
      try {
        const response = await fetch(
          `${import.meta.env.VITE_API}customer/${customer.id}`,
          {
            method: "PATCH",
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-Type": "application/json",
              "x-api-key": import.meta.env.VITE_API_KEY,
            },
            body: JSON.stringify({
              firstName: customerData.name,
              lastName: customerData.surname,
              username: values.username,
              password: values.password,
              address: customerData.address,
              paymentMethod: customerData.paymentMethod,
              cardInfo: {
                cardType: customerData.cardType,
                nameOnCard: customerData.nameOnCard,
                cardNumber: customerData.cardNumber,
                cardExpirationDate: customerData.expirationDate,
                cardCvv: customerData.cvv,
              },
              newPassword: "",
            }),
          }
        );
        if (response.ok) {
          const data = await response.json();
        } else {
          console.error("Failed to fetch customer data:", response.statusText);
        }
      } catch (error) {
        console.error("Error during customer data fetch:", error.message);
      }
    }
  };

  const onLoginSuccess = (values) => {
    setShowLoginPopup(false);
    saveCustomerDataToServer(values);
    console.log("values: ", values);
    console.log("customer data: ", customerData);
  };

  return (
    <div className="profile">
      <div className="container">
        
        <h2 className="account">Account</h2>
        <div className="option">
          <CustomerDataInput 
          profileScreen={true}
          setCustomerData={setCustomerData}
          />
        </div>
        
        <h2 className="payment">Payment</h2>
        <div className="option">  
          <CardInput profileScreen={true} setCustomerData={setCustomerData} />
          <button onClick={handleSaveButtonClicked}>SAVE</button>
        </div>

        {/* <div className="option">
          <h2 className="order-history">Order history</h2>
        </div> */}

          {showLoginPopup && (
            <ConfirmLoginDataPopup
              onLogin={onLoginSuccess}
              onClose={() => setShowLoginPopup(false)}
            />
          )}
      </div>
    </div>
      
  );
};

export default Profile;
