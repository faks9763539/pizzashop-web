import { useState, useRef, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import "./Admin.css";

const Product = ({
  product,
  index,
  products,
  setProducts,
  inventoryData,
  editedProducts,
  setEditedProducts,
}) => {
  const [imagePreview, setImagePreview] = useState("");
  const [selectedSizes, setSelectedSizes] = useState([]);
  const [selectedIngredients, setSelectedIngredients] = useState([]);
  const imageUrlRef = useRef();

  useEffect(() => {
    imageUrlRef.value = product.imageUrl;
    setSelectedSizes(product.size);
    setSelectedIngredients(product.ingredients || []);
    handleImage();
  }, []);

  const handleImage = () => {
    const imageUrl = imageUrlRef.current.value;
    const img = new Image();
    img.onload = () => {
      setImagePreview(imageUrl);
    };
    img.onerror = () => {
      setImagePreview(null);
    };
    img.src = imageUrl;

    handleImageChange(imageUrl);
  };

  const handleSizeChange = (size) => {
    const updatedSizes = selectedSizes.includes(size)
      ? selectedSizes.filter((s) => s !== size)
      : [...selectedSizes, size];

    setSelectedSizes(updatedSizes);

    const updatedProducts = [...products];
    updatedProducts[index] = {
      ...product,
      size: updatedSizes,
    };
    setProducts(updatedProducts);
    setEditedProducts([...editedProducts, updatedProducts[index]]);
  };

  const handlePriceChange = (newPrice) => {
    if (newPrice === 0 || newPrice === null) return;
    const updatedProducts = [...products];
    updatedProducts[index] = {
      ...product,
      price: newPrice,
    };
    setProducts(updatedProducts);
    setEditedProducts([...editedProducts, updatedProducts[index]]);
  };

  const handleImageChange = (newImageUrl) => {
    const updatedProducts = [...products];
    updatedProducts[index] = {
      ...product,
      imageUrl: newImageUrl,
    };
    setProducts(updatedProducts);
    setEditedProducts([...editedProducts, updatedProducts[index]]);
  };

  const handleIngredientChange = (ingredientName) => {
    const updatedIngredients = selectedIngredients.includes(ingredientName)
      ? selectedIngredients.filter(
          (ingredient) => ingredient !== ingredientName
        )
      : [...selectedIngredients, ingredientName];

    setSelectedIngredients(updatedIngredients);

    const updatedProducts = [...products];
    updatedProducts[index] = {
      ...product,
      ingredients: updatedIngredients,
    };
    setProducts(updatedProducts);
    setEditedProducts([...editedProducts, updatedProducts[index]]);
  };

  const deleteProduct = (productId) => {
    const updatedProducts = products.filter((item) => item._id !== productId);
    setProducts(updatedProducts);
    setEditedProducts([
      ...editedProducts,
      { _id: productId, deleted: true }, 
    ]);
  };

  return (
    <ul>
      <li key={product._id}>
        <p>Name: {product.name}</p>
        <label htmlFor="Product price">Price:</label>
        <input
          required
          type="text"
          value={product.price}
          onChange={(e) => handlePriceChange(e.target.value)}
          onBlur={(e) => handlePriceChange(parseFloat(e.target.value))}
        />
        <div>
          <label>Size:</label>
          <div>
            <input
              type="checkbox"
              checked={selectedSizes.includes("Small")}
              onChange={() => handleSizeChange("Small")}
            />
            <label htmlFor="sizeSmall">Small</label>
          </div>
          <div>
            <input
              type="checkbox"
              checked={selectedSizes.includes("Medium")}
              onChange={() => handleSizeChange("Medium")}
            />
            <label htmlFor="sizeMedium">Medium</label>
          </div>
          <div>
            <input
              type="checkbox"
              checked={selectedSizes.includes("Large")}
              onChange={() => handleSizeChange("Large")}
            />
            <label htmlFor="sizeLarge">Large</label>
          </div>
          <div>
            <input
              type="checkbox"
              checked={selectedSizes.includes("Jumbo")}
              onChange={() => handleSizeChange("Jumbo")}
            />
            <label htmlFor="sizeJumbo">Jumbo</label>
          </div>
        </div>
        <label>Ingredients:</label>
        {inventoryData && inventoryData.length > 0 && (
          <div>
            {inventoryData.map((ingredient, idx) => (
              <div key={idx}>
                <input
                  type="checkbox"
                  checked={selectedIngredients.includes(ingredient.name)}
                  onChange={() => handleIngredientChange(ingredient.name)}
                />
                <label htmlFor={`ingredient${idx}`}>{ingredient.name}</label>
              </div>
            ))}
          </div>
        )}
        <label htmlFor="Product imageUrl">Image URL:</label>
        <input
          required
          type="text"
          ref={imageUrlRef}
          onChange={handleImage}
          value={product.imageUrl}
        />
        {imagePreview && (
          <img
            src={imagePreview}
            alt="Product Preview"
            style={{ maxWidth: "50px", maxHeight: "50px" }}
          />
        )}
        <FontAwesomeIcon className="delete-icon"
          icon={faTrash}
          onClick={() => deleteProduct(product._id)}
        />
      </li>
    </ul>
  );
};

export default Product;
