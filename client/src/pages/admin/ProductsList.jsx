import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import ProductsPopup from "./ProductsPopup";
import Product from "./Product";
import "./Admin.css";

const ProductsList = ({ inventoryData }) => {
  const customer = useSelector((state) => state.customer);
  const token = useSelector((state) => state.token);
  const [addProductPopup, setAddProductPopup] = useState(false);
  const [products, setProducts] = useState([]);
  const [editedProducts, setEditedProducts] = useState([]);

  const toggleAddProductsPopup = () => {
    setAddProductPopup(!addProductPopup);
  };

  const getProducts = async () => {
    try {
      const response = await fetch(`${import.meta.env.VITE_API}products`);
      if (response.ok) {
        const data = await response.json();
        setProducts(data);
      } else {
        console.error("Failed to fetch products:", response.statusText);
      }
    } catch (error) {
      console.error("Error during product fetch:", error.message);
    }
  };

  useEffect(() => {
    getProducts();
  }, []);

  const saveChanges = async () => {
    try {
      for (const product of editedProducts) {
        if (product.deleted) {
          // Delete product
          const response = await fetch(
            `${import.meta.env.VITE_API}products/${product._id}`,
            {
              method: "DELETE",
              headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json",
                "x-api-key": import.meta.env.VITE_API_KEY,
              },
              body: JSON.stringify({
                customerId: customer.id,
              }),
            }
          );
        } else {
          // Update product
          const response = await fetch(
            `${import.meta.env.VITE_API}products/${product._id}`,
            {
              method: "PATCH",
              headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json",
                "x-api-key": import.meta.env.VITE_API_KEY,
              },
              body: JSON.stringify({
                customerId: customer.id,
                name: product.name,
                price: product.price,
                size: product.size,
                imageUrl: product.imageUrl,
                ingredients: product.ingredients,
              }),
            }
          );
        }
      }
      // Clear edited products after successful save
      setEditedProducts([]);
      alert("Changes saved successfully");
    } catch (error) {
      alert("Error");
    }
  };

  return (
    <div className="admin-products-list">
      <button onClick={() => toggleAddProductsPopup()}>Add new +</button>
      {products.length > 0 ? (
        products.map((product, index) => (
          <Product
            key={index}
            product={product}
            index={index}
            products={products}
            setProducts={setProducts}
            inventoryData={inventoryData}
            editedProducts={editedProducts}
            setEditedProducts={setEditedProducts}
          />
        ))
      ) : (
        <p>No inventory data available</p>
      )}
      <button onClick={saveChanges}>Save</button>

      {addProductPopup && (
        <ProductsPopup
          toggleAddProductsPopup={toggleAddProductsPopup}
          inventoryData={inventoryData}
        />
      )}
    </div>
  );
};

export default ProductsList;
