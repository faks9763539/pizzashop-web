import React, { useRef } from "react";
import { useSelector } from "react-redux";
import "./Admin.css";

const IngredientPopup = ({ toggleAddIngredientPopup }) => {
  const unitRef = useRef();
  const nameRef = useRef();
  const quantityRef = useRef();
  const customer = useSelector((state) => state.customer);

  const handleAddButtonClick = () => {
    const selectedUnit = unitRef.current.value;
    const ingredientName = nameRef.current.value;
    const ingredientQuantity = quantityRef.current.value;

    const newIngredient = {
      name: ingredientName,
      quantity: ingredientQuantity,
      unit: selectedUnit,
    };
    console.log("Added", newIngredient);
    addIngredientToServer(newIngredient);

    window.location.reload(false);

    toggleAddIngredientPopup();
  };

  const addIngredientToServer = async (newIngredient) => {
    try {
      const response = await fetch(`${import.meta.env.VITE_API}inventory`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${customer.token}`,
          "x-api-key": import.meta.env.VITE_API_KEY,
        },
        body: JSON.stringify({
          customerId: customer.id,
          ...newIngredient,
        }),
      });

      if (response.ok) {
        console.log("Ingredient added successfully to the server");
      } else {
        console.error(
          "Failed to add ingredient to the server:",
          response.statusText
        );
      }
    } catch (error) {
      console.error("Error during ingredient addition:", error.message);
    }
  };

  return (
    <div className="popup-parent">
      <div className="popup-content">
        <button className="x-btn" onClick={() => toggleAddIngredientPopup()}>X</button>
        <div className="popup-form">
          <label htmlFor="Ingredient name">Name:</label>
          <input required type="text" ref={nameRef} />

          <label htmlFor="Ingredient quantity">Quantity:</label>
          <input required type="number" ref={quantityRef} />

          <label htmlFor="Ingredient unit">Unit:</label>
          <select name="unit" id="unit" ref={unitRef}>
            <option value="Kg">Kg</option>
            <option value="L">L</option>
          </select>

          <button className="add-btn" onClick={handleAddButtonClick}>ADD</button>
        </div>
      </div>
    </div>
  );
};

export default IngredientPopup;
