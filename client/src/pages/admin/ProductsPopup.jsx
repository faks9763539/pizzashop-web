import { useRef, useState } from "react";
import { useSelector } from "react-redux";
import "./Admin.css";

const ProductsPopup = ({ toggleAddProductsPopup, inventoryData }) => {
  const nameRef = useRef();
  const priceRef = useRef();
  const imageUrlRef = useRef();
  const customer = useSelector((state) => state.customer);
  const [imagePreview, setImagePreview] = useState(null);
  const [selectedSizes, setSelectedSizes] = useState([]);
  const [ingredients, setIngredients] = useState([]);

  const handleImageChange = () => {
    const imageUrl = imageUrlRef.current.value;
    const img = new Image();
    img.onload = () => {
      setImagePreview(imageUrl);
    };
    img.onerror = () => {
      setImagePreview(null);
    };
    img.src = imageUrl;
  };

  const handleIngredientChange = (ingredientName) => {
    const updatedIngredients = ingredients.includes(ingredientName)
      ? ingredients.filter((ingredient) => ingredient !== ingredientName)
      : [...ingredients, ingredientName];

    setIngredients(updatedIngredients);
  };

  const handleSizeChange = (size) => {
    const updatedSizes = selectedSizes.includes(size)
      ? selectedSizes.filter((s) => s !== size)
      : [...selectedSizes, size];

    setSelectedSizes(updatedSizes);
  };

  const handleAddButtonClick = () => {
    const productName = nameRef.current.value;
    const productPrice = priceRef.current.value;
    const productImageUrl = imageUrlRef.current.value;
    if (selectedSizes.length === 0) return;
    const newProduct = {
      name: productName,
      price: productPrice,
      size: selectedSizes,
      imageUrl: productImageUrl,
      ingredients: ingredients,
    };

    addProductToServer(newProduct);
  };

  const addProductToServer = async (newProduct) => {
    try {
      const response = await fetch(`${import.meta.env.VITE_API}products`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${customer.token}`,
          "x-api-key": import.meta.env.VITE_API_KEY,
        },
        body: JSON.stringify({
          customerId: customer.id,
          ...newProduct,
        }),
      });

      if (response.ok) {
        console.log("Product added successfully to the server");
        toggleAddProductsPopup();
        window.location.reload(false);
      } else {
        console.error(
          "Failed to add product to the server:",
          response.statusText
        );
      }
    } catch (error) {
      console.error("Error during product addition:", error.message);
    }
  };

  return (
    <div className="popup-parent">
      <div className="popup-content">
        <button className="x-btn" onClick={() => toggleAddProductsPopup()}>X</button>
        <div className="popup-form">
          <label htmlFor="Product Ingredient name">Name:</label>
          <input required type="text" ref={nameRef} />

          <label htmlFor="Product quantity">Price:</label>
          <input required type="number" ref={priceRef} />

          <label>Size:</label>
          <div>
            <input
              type="checkbox"
              checked={selectedSizes.includes("Small")}
              onChange={() => handleSizeChange("Small")}
            />
            <label htmlFor="sizeSmall">Small</label>
          </div>
          <div>
            <input
              type="checkbox"
              checked={selectedSizes.includes("Medium")}
              onChange={() => handleSizeChange("Medium")}
            />
            <label htmlFor="sizeMedium">Medium</label>
          </div>
          <div>
            <input
              type="checkbox"
              checked={selectedSizes.includes("Large")}
              onChange={() => handleSizeChange("Large")}
            />
            <label htmlFor="sizeLarge">Large</label>
          </div>
          <div>
            <input
              type="checkbox"
              checked={selectedSizes.includes("Jumbo")}
              onChange={() => handleSizeChange("Jumbo")}
            />
            <label htmlFor="sizeJumbo">Jumbo</label>
          </div>
          <div>
            <label>Ingredients:</label>
            {inventoryData.map((ingredient, index) => (
              <div key={index}>
                <input
                  type="checkbox"
                  checked={ingredients.includes(ingredient.name)}
                  onChange={() => handleIngredientChange(ingredient.name)}
                />
                <label htmlFor={`ingredient${index}`}>{ingredient.name}</label>
              </div>
            ))}
          </div>
          <label htmlFor="Product imageUrl">Image URL:</label>
          <input
            required
            type="text"
            ref={imageUrlRef}
            onChange={handleImageChange}
          />
          {imagePreview && (
            <img
              src={imagePreview}
              alt="Product Preview"
              style={{ maxWidth: "100%", maxHeight: "200px", margin: "10px 0" }}
            />
          )}
          <button className="add-btn" onClick={handleAddButtonClick}>ADD</button>
        </div>
      </div>
    </div>
  );
};

export default ProductsPopup;
