import React, { useState } from "react";
import { useSelector } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import IngredientPopup from "./IngredientPopup";

const IngredientList = ({ inventoryData, setInventoryData }) => {
  const [addIngredientPopup, setAddIngredientPopup] = useState(false);
  const [editedIngredients, setEditedIngredients] = useState([]);
  const customer = useSelector((state) => state.customer);
  const token = useSelector((state) => state.token);

  const toggleAddIngredientPopup = () => {
    setAddIngredientPopup(!addIngredientPopup);
  };

  const updateIngredient = (index, newUnit) => {
    const updatedInventoryData = [...inventoryData];
    updatedInventoryData[index] = {
      ...updatedInventoryData[index],
      unit: newUnit,
    };
    setInventoryData(updatedInventoryData);
    setEditedIngredients([...editedIngredients, updatedInventoryData[index]]);
  };

  const handleAmountChange = (index, newQuantity) => {
    const updatedInventoryData = [...inventoryData];
    updatedInventoryData[index] = {
      ...updatedInventoryData[index],
      quantity: newQuantity,
    };
    setInventoryData(updatedInventoryData);
    setEditedIngredients([...editedIngredients, updatedInventoryData[index]]);
  };

  const deleteIngredient = (ingredientId) => {
    const updatedInventoryData = inventoryData.filter(
      (item) => item._id !== ingredientId
    );
    setInventoryData(updatedInventoryData);
    setEditedIngredients([
      ...editedIngredients,
      { _id: ingredientId, deleted: true }, // Mark as deleted
    ]);
  };

  const saveChanges = async () => {
    try {
      for (const ingredient of editedIngredients) {
        if (ingredient.deleted) {
          // Delete ingredient
          const response = await fetch(
            `${import.meta.env.VITE_API}inventory/${ingredient._id}`,
            {
              method: "DELETE",
              headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json",
                "x-api-key": import.meta.env.VITE_API_KEY,
              },
              body: JSON.stringify({
                customerId: customer.id,
              }),
            }
          );
        } else {
          // Update ingredient
          const response = await fetch(
            `${import.meta.env.VITE_API}inventory/${ingredient._id}`,
            {
              method: "PATCH",
              headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json",
                "x-api-key": import.meta.env.VITE_API_KEY,
              },
              body: JSON.stringify({
                customerId: customer.id,
                name: ingredient.name,
                quantity: ingredient.quantity,
                unit: ingredient.unit,
              }),
            }
          );
        }
      }
      // Clear edited ingredients after successful save
      setEditedIngredients([]);
      alert("Changes saved successfully");
    } catch (error) {
      alert("Error");
    }
  };

  return (
    <div className="inventory">
      <button onClick={toggleAddIngredientPopup}>Add new +</button>
      {inventoryData.length > 0 ? (
        <ul>
          {inventoryData.map((ingredient, index) => (
            <li key={ingredient._id}>
              <p>Name: {ingredient.name}</p>
              <label htmlFor="Ingredient name">Quantity:</label>
              <input
                required
                type="number"
                value={ingredient.quantity}
                onChange={(e) => handleAmountChange(index, e.target.value)}
                min="0"
              />
              <div className="dropdown">
                <button className="dropbtn">{ingredient.unit}</button>
                <div className="dropdown-content">
                  <a onClick={() => updateIngredient(index, "Kg")}>Kg</a>
                  <a onClick={() => updateIngredient(index, "L")}>L</a>
                </div>
              </div>
              <FontAwesomeIcon
                icon={faTrash}
                onClick={() => deleteIngredient(ingredient._id)}
              />
            </li>
          ))}
        </ul>
      ) : (
        <p>No inventory data available</p>
      )}
      <button onClick={saveChanges}>Save</button>
      {addIngredientPopup && (
        <IngredientPopup toggleAddIngredientPopup={toggleAddIngredientPopup} />
      )}
    </div>
  );
};

export default IngredientList;
