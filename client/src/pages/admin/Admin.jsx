import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import ProductsList from "./ProductsList";
import IngredientList from "./IngredientList";
import "./Admin.css";

const Admin = () => {
  const navigate = useNavigate();
  const [inventoryData, setInventoryData] = useState([]);
  const customer = useSelector((state) => state.customer);
  const token = useSelector((state) => state.token);
  const isLoggedIn = useSelector((state) => state.customer !== null);
  const isAdmin = useSelector(
    (state) => (state.customer && state.customer.isAdmin) || false
  );

  const fetchInventoryData = async () => {
    try {
      const response = await fetch(
        `${import.meta.env.VITE_API}inventory/${customer.id}`,
        {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
            "x-api-key": import.meta.env.VITE_API_KEY,
          },
        }
      );

      if (response.ok) {
        const data = await response.json();
        setInventoryData(data);
      } else {
        console.error("Failed to fetch inventory data:", response.statusText);
      }
    } catch (error) {
      console.error("Error during inventory fetch:", error.message);
    }
  };

  useEffect(() => {
    if (customer && token) {
      fetchInventoryData();
    }

    if (!isLoggedIn || !isAdmin) {
      navigate("/");
    }
  }, [customer, token]);

  if (!isLoggedIn || !isAdmin) return <></>;

  return (
    <>
      <div className="admin">
        <h1 className="admin-main-title">Inventory</h1>
        <div className="admin-subtitles">
          <h2>Ingredients</h2>
          <h2>Products</h2>
        </div> 
        <div className="admin-inventory">
          <IngredientList
            inventoryData={inventoryData}
            setInventoryData={setInventoryData}
          />
          <ProductsList inventoryData={inventoryData} />
        </div>
      </div>
    </>
  );
};

export default Admin;
