import React, { useEffect, useState, forwardRef } from "react";
import { useNavigate } from "react-router-dom";
import Product from "../../components/product/Product";
import "./SpecialOffer.css";

const SpecialOffer = forwardRef((props, ref) => {

  const navigate = useNavigate();
  const [products, setProducts] = useState([]);

  const getProducts = async () => {
    try {
      const response = await fetch(`${import.meta.env.VITE_API}products`);
      if (response.ok) {
        const data = await response.json();
        setProducts(data);
      } else {
        console.error("Failed to fetch products:", response.statusText);
      }
    } catch (error) {
      console.error("Error during product fetch:", error.message);
    }
  };

  useEffect(() => {
    getProducts();
  }, []);

  return (
    <div className="special-offers" ref={ref}>
      <h1 className="title">SPECIAL OFFERS</h1>
      <div className="product-list">
        {products.slice(0, 3).map((product) => (
          <Product key={product._id} product={product} />
        ))}
      </div>
      <a onClick={() => navigate("/menu")} className="go-to-menu">
        See all...
      </a>
    </div>
  );
});

export default SpecialOffer;