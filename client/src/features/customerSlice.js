import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  customer: null,
  token: null,
  cart: [],
  guest: false,
  stayLoggedIn: false,
};

export const customerSlice = createSlice({
  name: "customer",
  initialState,
  reducers: {
    setStayLoggedIn: (state, action) => {
      state.stayLoggedIn = action.payload;
    },
    setLogin: (state, action) => {
      state.customer = action.payload.customer;
      state.token = action.payload.token;
      state.cart = action.payload.cart;
      state.guest = action.payload.guest;

      if (!state.stayLoggedIn) {
        setTimeout(() => {
          localStorage.removeItem("persist:root");
        }, 100);
      }
    },
    setLogout: (state) => {
      state.customer = null;
      state.token = null;
      state.cart = [];
      state.guest = false;
    },
    setGuest: (state, action) => {
      state.guest = action.payload.guest;
    },
    addToCart: (state, action) => {
      const { name, size, amount, _id } = action.payload;
      const existingItem = state.cart.find(
        (item) => item._id === _id && item.size === size
      );

      if (existingItem) {
        existingItem.amount += amount;
      } else {
        state.cart.push({
          name,
          size,
          amount,
          _id,
        });
      }
      if (state.customer && state.token) {
        saveCartServer(state);
      }
    },
    clearCart: (state) => {
      state.cart = [];
      if (state.customer && state.token) {
        saveCartServer(state);
      }
    },
    updateCartItemAmount: (state, action) => {
      const { name, size, amount, _id } = action.payload;
      const existingIndex = state.cart.findIndex(
        (item) => item._id === _id && item.size === size
      );

      if (amount === 0) {
        if (existingIndex !== -1) {
          state.cart.splice(existingIndex, 1);
        }
      } else {
        if (existingIndex !== -1) {
          state.cart[existingIndex].amount = amount;
        } else {
          state.cart.push({
            name,
            size,
            amount,
            _id,
          });
        }
      }
      if (state.customer && state.token) {
        saveCartServer(state);
      }
    },
    updateCartItemSize: (state, action) => {
      const { name, size, amount, _id } = action.payload;
      const existingItem = state.cart.find(
        (item) => item._id === _id && item.amount === amount
      );

      if (existingItem) {
        existingItem.size = size;

        const sameItems = state.cart.filter(
          (item) => item._id === _id && item.size === size
        );

        if (sameItems.length >= 2) {
          state.cart = state.cart.filter(
            (item) => !(item._id === _id && item.size === size)
          );

          const sumAmount = sameItems.reduce(
            (sum, item) => sum + item.amount,
            0
          );

          state.cart.push({
            name,
            size,
            amount: sumAmount,
            _id,
          });
        }
      }

      if (state.customer && state.token) {
        saveCartServer(state);
      }
    },
  },
});

const saveCartServer = async (state) => {
  const { customer, token, cart } = state;
  try {
    const response = await fetch(
      `${import.meta.env.VITE_API}cart/${customer.id}/`,
      {
        method: "PATCH",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
          "x-api-key": import.meta.env.VITE_API_KEY,
        },
        body: JSON.stringify({ items: cart }),
      }
    );
    const success = await response.json();
  } catch (error) {
    console.error("Error saving cart to server: ", error.message);
  }
};

export const {
  setStayLoggedIn,
  setLogin,
  setLogout,
  setGuest,
  addToCart,
  clearCart,
  updateCartItemSize,
  updateCartItemAmount,
} = customerSlice.actions;

export default customerSlice.reducer;
