import { useRef } from "react";
import {
  BrowserRouter as Router,
  Navigate,
  Routes,
  Route,
} from "react-router-dom";
import "./App.css";
import Navbar from "./components/navbar/Navbar";
import Layout from "./pages/layout/Layout";
import Cart from "./pages/cart/Cart";
import Menu from "./pages/menu/Menu";
import Checkout from "./pages/checkout/Checkout";
import Admin from "./pages/admin/Admin";
import Profile from "./pages/profile/Profile";
import Tracking from "./pages/tracking/Tracking";
function App() {
  const homeRef = useRef(null);
  const specialOfferRef = useRef(null);
  const aboutRef = useRef(null);
  const contactRef = useRef(null);

  return (
    <div className="app">
      <Router>
        <Navbar
          homeRef={homeRef}
          specialOfferRef={specialOfferRef}
          aboutRef={aboutRef}
          contactRef={contactRef}
        />
        <Routes>
          <Route
            path="/"
            element={
              <Layout
                homeRef={homeRef}
                specialOfferRef={specialOfferRef}
                aboutRef={aboutRef}
                contactRef={contactRef}
              />
            }
          />
          <Route path="/menu" element={<Menu />} />
          <Route path="/cart" element={<Cart />} />
          <Route path="/checkout" element={<Checkout />} />
          <Route path="/tracking/:orderId" element={<Tracking />} />
          <Route path="/customer/:customerId" element={<Profile />} />
          <Route path="/admin" element={<Admin />} />
          <Route path="/*" element={<Navigate to="/" />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
