import React, { useState, useEffect } from "react";
import { NavLink, useNavigate, useLocation } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faUser,
  faShoppingCart,
  faUserGear,
} from "@fortawesome/free-solid-svg-icons";
import Auth from "../auth/auth";
import { setLogout } from "../../features/customerSlice";

import "./Navbar.css";
import image from "../../assets/Logo.png";

const Navbar = ({ homeRef, specialOfferRef, aboutRef, contactRef }) => {
  const [currentRef, setCurrentRef] = useState(null);
  const navigate = useNavigate();
  const location = useLocation();
  const [showAuthOverlay, setShowAuthOverlay] = useState(false);
  const isLoggedIn = useSelector((state) => state.customer !== null);
  const isAdmin = useSelector(
    (state) => (state.customer && state.customer.isAdmin) || false
  );
  const customer = useSelector((state) => state.customer);
  const dispatch = useDispatch();

  const handleAuthButtonClick = () => {
    setShowAuthOverlay(true);
  };

  const closeAuthOverlay = () => {
    setShowAuthOverlay(false);
  };

  const handleLogoutClick = () => {
    dispatch(setLogout());
    navigate("/");
  };

  const scrollToSection = (elementRef) => {
    navigate("/");
    setTimeout(() => {
      setCurrentRef(elementRef);
      elementRef.current.scrollIntoView({ behavior: "smooth" });
    }, 5);
  };

  useEffect(() => {
    if (location.pathname === "/") {
      setCurrentRef(homeRef);
    } else {
      setCurrentRef(null);
    }
  }, [location.pathname, homeRef]);

  return (
    <div>
      <nav className="navbar">
          <div>
            <img src={image} alt=""/>
          </div>
        <ul className="nav-list">
          <li>
            <div
              className={currentRef === homeRef ? "active" : "nav-link"}
              onClick={() => scrollToSection(homeRef)}
            >
              HOME
            </div>
          </li>
          <li>
            <div
              className={currentRef === specialOfferRef ? "active" : "nav-link"}
              onClick={() => scrollToSection(specialOfferRef)}
            >
              SPECIAL OFFER
            </div>
          </li>
          <li>
            <div
              className={currentRef === aboutRef ? "active" : "nav-link"}
              onClick={() => scrollToSection(aboutRef)}
            >
              ABOUT
            </div>
          </li>
          <li>
            <div
              className={currentRef === contactRef ? "active" : "nav-link"}
              onClick={() => scrollToSection(contactRef)}
            >
              CONTACT
            </div>
          </li>
          <li>
            <NavLink
              to="/menu"
              className={(navData) =>
                navData.isActive ? "active" : "nav-link"
              }
              onClick={() => setCurrentRef(null)}
            >
              MENU
            </NavLink>
          </li>
          <div className="navbar-login-cart">
            <li>
              {isLoggedIn ? (
                <button onClick={handleLogoutClick}>
                  LOGOUT
                </button>
              ) : (
                <button onClick={handleAuthButtonClick}>
                  LOGIN / REGISTER
                </button>
              )}
            </li>

            {isAdmin ? (
              <li>
                <NavLink
                  to="/admin"
                  className={(navData) =>
                    navData.isActive ? "active" : "nav-link"
                  }
                  onClick={() => setCurrentRef(null)}
                >
                  <FontAwesomeIcon icon={faUserGear} />
                </NavLink>
              </li>
            ) : customer ? (
              <li>
                <NavLink
                  to={`/customer/${customer.id}`}
                  className={(navData) =>
                    navData.isActive ? "active" : "nav-link"
                  }
                  onClick={() => setCurrentRef(null)}
                >
                  <FontAwesomeIcon icon={faUser} />
                </NavLink>
              </li>
            ) : null}
            {!isAdmin && (
              <li>
                <NavLink
                  to="/cart"
                  className={(navData) =>
                    navData.isActive ? "active" : "nav-link"
                  }
                  onClick={() => setCurrentRef(null)}
                >
                  <FontAwesomeIcon className="cart-icon" icon={faShoppingCart} />
                </NavLink>
              </li>
            )}
          </div>
        </ul>
      </nav>

      {showAuthOverlay && <Auth onClose={closeAuthOverlay} />}
    </div>
  );
};

export default Navbar;
