// CardInput.jsx
import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import "./CardInput.css";

const CardInput = ({ setCustomerData, profileScreen = false }) => {
  const isLoggedIn = useSelector((state) => state.customer !== null);
  const customer = useSelector((state) => state.customer);
  const token = useSelector((state) => state.token);
  const [paymentMethod, setPaymentMethod] = useState("cash");
  const [cardInfo, setCardInfo] = useState({
    cardType: "",
    nameOnCard: "",
    cardNumber: "",
    expirationDate: "",
    cvv: "",
  });

  useEffect(() => {
    getCustomerDataFromServer();
  }, []);

  const handlePaymentMethodChange = (event) => {
    setPaymentMethod(event.target.value);
    if (profileScreen) {
      setCustomerData((prevData) => ({
        ...prevData,
        ...cardInfo,
        paymentMethod,
      }));
    }
  };

  const handleCardInfoChange = (event) => {
    const { name, value } = event.target;

    setCardInfo((prevCardInfo) => ({ ...prevCardInfo, [name]: value }));
  };

  useEffect(() => {
    if (profileScreen) {
      setCustomerData((prevData) => ({
        ...prevData,
        ...cardInfo,
        paymentMethod,
      }));
    }
  }, [cardInfo, profileScreen, paymentMethod]);

  const getCustomerDataFromServer = async () => {
    if (isLoggedIn) {
      try {
        const response = await fetch(
          `${import.meta.env.VITE_API}customer/${customer.id}`,
          {
            method: "GET",
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-Type": "application/json",
              "x-api-key": import.meta.env.VITE_API_KEY,
            },
          }
        );

        if (response.ok) {
          const data = await response.json();
          setPaymentMethod(data.paymentMethod.toLowerCase());
          setCardInfo({
            cardType: data.cardInfo.cardType,
            nameOnCard: data.cardInfo.nameOnCard,
            cardNumber: data.cardInfo.cardNumber,
            expirationDate: data.cardInfo.cardExpirationDate,
            cvv: data.cardInfo.cardCvv,
          });
          if (profileScreen) {
            setCustomerData((prevData) => ({
              ...prevData,
              cardType: data.cardInfo.cardType,
              nameOnCard: data.cardInfo.nameOnCard,
              cardNumber: data.cardInfo.cardNumber,
              expirationDate: data.cardInfo.cardExpirationDate,
              cvv: data.cardInfo.cardCvv,
              paymentMethod: paymentMethod,
            }));
          }
        } else {
          console.error("Failed to fetch customer data:", response.statusText);
        }
      } catch (error) {
        console.error("Error during customer data fetch:", error.message);
      }
    }
  };

  return (
    <div
      className={
        profileScreen
          ? "card-input profile-screen"
          : "card-input checkout-screen"
      }
    >
      <label htmlFor="paymentMethod">Choose a payment method:</label>
      <select
        id="paymentMethod"
        name="paymentMethod"
        value={paymentMethod}
        onChange={handlePaymentMethodChange}
      >
        <option value="cash">Cash</option>
        <option value="card">Card</option>
      </select>

      {paymentMethod === "card" && (
        <div
          className={
            profileScreen
              ? "card-info profile-screen"
              : "card-info checkout-screen"
          }
        >
          <label htmlFor="cardType">Type of Card:</label>
          <input
            placeholder="Visa, Master, Maestro..."
            type="text"
            id="cardType"
            name="cardType"
            value={cardInfo.cardType || ""}
            onChange={handleCardInfoChange}
            required
          />
          <label htmlFor="cardName">Name on Card:</label>
          <input
            placeholder="John Dough"
            type="text"
            id="cardName"
            name="nameOnCard"
            value={cardInfo.nameOnCard || ""}
            onChange={handleCardInfoChange}
            required
          />

          <label htmlFor="cardNumber">Card Number:</label>
          <input
            placeholder="1234 1234 1234 1234"
            type="text"
            id="cardNumber"
            name="cardNumber"
            value={cardInfo.cardNumber || ""}
            onChange={handleCardInfoChange}
            required
          />

          <label htmlFor="expirationMonth">Expiration Month:</label>
          <input
            placeholder="0124"
            type="text"
            id="expirationMonth"
            name="expirationDate"
            value={cardInfo.expirationDate || ""}
            onChange={handleCardInfoChange}
            required
          />

          {!profileScreen && (
            <>
              <label htmlFor="cvv">CVV:</label>
              <input
                placeholder="123"
                type="text"
                id="cvv"
                name="cvv"
                value={cardInfo.cvv || ""}
                onChange={handleCardInfoChange}
                required
              />
            </>
          )}
        </div>
      )}
    </div>
  );
};

export default CardInput;
