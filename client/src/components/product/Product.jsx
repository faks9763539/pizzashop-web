import React, { useState } from "react";
import ProductPopup from "../productPopup/ProductPopup";
import { useDispatch } from "react-redux";
import { addToCart } from "../../features/customerSlice";
import "./Product.css";
const Product = ({ product }) => {
  const [showPopup, setShowPopup] = useState(false);
  const dispatch = useDispatch();

  const openPopup = () => {
    setShowPopup(true);
  };

  const closePopup = () => {
    setShowPopup(false);
  };

  const addItemToCart = (product, size, amount) => {
    const { name, _id } = product;
    console.log(_id);
    dispatch(addToCart({ name, size, amount, _id }));
    closePopup();
  };

  return (
    <div className="product">
      <img src={product.imageUrl} alt={product.name} className="product-img"/>
      <h2>{product.name}</h2>
      <p>Price: €{product.price.toFixed(2)}</p>
      {product.size.length > 0 ? (
        <>
          <button className="add-to-cart-button" onClick={openPopup}>Add to Cart</button>

          <ProductPopup
            product={product}
            show={showPopup}
            onClose={closePopup}
            onAddToCart={addItemToCart}
          />
        </>
      ) : (
        <div>Unavailable</div>
      )}
    </div>
  );
};

export default Product;
