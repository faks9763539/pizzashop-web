import React, { useState, useEffect } from "react";
import "./ProductPopup.css";

const ProductPopup = ({
  product: selectedProduct,
  show,
  onClose,
  onAddToCart,
}) => {
  const [selectedSize, setSelectedSize] = useState(null);
  const [selectedAmount, setSelectedAmount] = useState(1);

  const addToCart = () => {
    if (selectedSize) {
      onAddToCart(selectedProduct, selectedSize, selectedAmount);
      setSelectedSize(selectedProduct.size[0]);
      setSelectedAmount(1);
      onClose();
    }
  };

  return (
    show && (
      <div className="popup">
        <h2>Select Size and Quantity</h2>
        <p>Selected Product: {selectedProduct.name}</p>
        <p>Price: €{selectedProduct.price.toFixed(2)}</p>
        <img src={selectedProduct.imageUrl} alt={selectedProduct.name} />
        <form>
          <p>Available Sizes:</p>
          {selectedProduct.size.map((size) => (
            <label key={size} className="size-label">
              <input
                type="radio"
                name="size"
                value={size}
                checked={size === selectedSize}
                onChange={() => setSelectedSize(size)}
              />
              {size}
            </label>
          ))}
          <hr />
          <label>
            Quantity:
            <input
              type="number"
              min="1"
              value={selectedAmount}
              onChange={(e) => setSelectedAmount(parseInt(e.target.value, 10))}
            />
          </label>
        </form>
        <button onClick={addToCart} className="add-to-cart-btn">
          Add to Cart
        </button>
        <button onClick={onClose} className="cancel-btn">
          Cancel
        </button>
      </div>
    )
  );
};

export default ProductPopup;
