import "./DeletionPopup.css";

const DeletionPopup = ({ onDelete, onCancel }) => (
  <div className="delete-item-popup">
    <div className="delete-item-popup-content">
      <p>Are you sure you want to delete this item?</p>
      <button onClick={onDelete}>Yes</button>
      <button onClick={onCancel}>No</button>
    </div>
  </div>
);

export default DeletionPopup;
