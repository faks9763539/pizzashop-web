import "./CustomerDataInput.css";
import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addToCart } from "../../features/customerSlice";

const CustomerDataInput = ({ setCustomerData, profileScreen = false }) => {
  const dispatch = useDispatch();
  const isLoggedIn = useSelector((state) => state.customer !== null);
  const customer = useSelector((state) => state.customer);
  const token = useSelector((state) => state.token);
  const [unavailableProducts, setUnavailableProducts] = useState([]);
  const [products, setProducts] = useState([]);

  const [customerInfo, setCustomerInfo] = useState({
    name: "",
    surname: "",
    address: "",
    orderHistory: [],
  });

  const getProducts = async () => {
    try {
      const response = await fetch(`${import.meta.env.VITE_API}products`);
      if (response.ok) {
        const data = await response.json();
        setProducts(data);
      } else {
        console.error("Failed to fetch products:", response.statusText);
      }
    } catch (error) {
      console.error("Error during product fetch:", error.message);
    }
  };

  useEffect(() => {
    if (profileScreen) {
      getProducts();
    }
    getCustomerDataFromServer();
  }, []);

  const getCustomerDataFromServer = async () => {
    if (isLoggedIn) {
      try {
        const response = await fetch(
          `${import.meta.env.VITE_API}customer/${customer.id}`,
          {
            method: "GET",
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-Type": "application/json",
              "x-api-key": import.meta.env.VITE_API_KEY,
            },
          }
        );

        if (response.ok) {
          const data = await response.json();
          setCustomerInfo({
            name: data.firstName,
            surname: data.lastName,
            address: data.address,
            orderHistory: data.orderHistory,
          });
          if (profileScreen) {
            setCustomerData((prevData) => ({
              ...prevData,
              name: data.firstName,
              surname: data.lastName,
              address: data.address,
            }));
          }
        } else {
          console.error("Failed to fetch customer data:", response.statusText);
        }
      } catch (error) {
        console.error("Error during customer data fetch:", error.message);
      }
    }
  };

  const handleCustomerInfoChange = (event) => {
    const { name, value } = event.target;

    setCustomerInfo((prevCustomerInfo) => ({
      ...prevCustomerInfo,
      [name]: value,
    }));
  };

  useEffect(() => {
    if (profileScreen) {
      setCustomerData((prevData) => ({ ...prevData, ...customerInfo }));
    }
  }, [profileScreen, customerInfo]);

  const getOrderTotalPrice = (orderItems) => {
    return orderItems
      .reduce((total, item) => {
        const productPrice = getProductPrice(item.name, item.size);
        return total + item.amount * productPrice;
      }, 0)
      .toFixed(2);
  };

  const getProductPrice = (productName, pizzaSize) => {
    const size = typeof pizzaSize === "string" ? pizzaSize.toLowerCase() : "";
    const product = products.find((p) => p.name === productName);
    if (!product) {
      return 0;
    }

    let sizeAdjustment = 0;

    switch (size) {
      case "medium":
        sizeAdjustment = 1;
        break;
      case "large":
        sizeAdjustment = 2;
        break;
      case "jumbo":
        sizeAdjustment = 3;
        break;
      default:
        sizeAdjustment = 0;
    }

    return product.price + sizeAdjustment;
  };

  const addToCartFromOrder = (order) => {
    setUnavailableProducts([]);

    order.items.forEach((item) => {
      const { name, size, amount } = item;
      const product = products.find((product) => product.name === name);

      if (product && product.size.includes(size)) {
        dispatch(addToCart({ name, size, amount }));
      } else {
        setUnavailableProducts((prevUnavailableProducts) => [
          ...prevUnavailableProducts,
          { name, size },
        ]);
      }
    });
  };

  return (
    <div
      className={
        profileScreen
          ? "customer-data-input profile-screen"
          : "customer-data-input checkout-screen"
      }
    >
      <label htmlFor="name">Name:</label>
      <input
        placeholder="Name"
        type="text"
        id="name"
        name="name"
        value={customerInfo.name}
        onChange={handleCustomerInfoChange}
        required
      />

      <label htmlFor="surname">Surname:</label>
      <input
        placeholder="Surname"
        type="text"
        id="surname"
        name="surname"
        value={customerInfo.surname}
        onChange={handleCustomerInfoChange}
        required
      />

      <label htmlFor="address">Address:</label>
      <input
        placeholder="Address"
        type="text"
        id="address"
        name="address"
        value={customerInfo.address}
        onChange={handleCustomerInfoChange}
        required
      />
      {profileScreen && (
        <>
          <div>
            <label htmlFor="orderHistory">Order History:</label>
            {customerInfo.orderHistory.map((order, index) => (
              <div className="orders-list" key={index} href="#">
                <div className="order-details">
                  <p className="order">Order: {order.orderId}</p>

                {order.items?.map((items, index) => (
                  <p key={index}>
                    {items.amount}x {items.name}, {items.size}
                  </p>
                ))}
                <p className="price">{getOrderTotalPrice(order.items)}€</p>
                </div>
                <div className="order-again">
                  <button onClick={() => addToCartFromOrder(order)}>
                    Order again
                  </button>
                </div> 
              </div>
            ))}
          </div>
          {unavailableProducts.length > 0 && (
            <div className="popup">
              <p>{`The following products are currently unavailable:`}</p>
              <ul>
                {unavailableProducts.map((product, index) => (
                  <li
                    key={index}
                  >{`${product.name}, Size: ${product.size}`}</li>
                ))}
              </ul>
              <button onClick={() => setUnavailableProducts([])}>Close</button>
            </div>
          )}
        </>
      )}
    </div>
  );
};

export default CustomerDataInput;
