import React, { useState } from "react";
import "./ConfirmLoginDataPopup.css";

const LoginPopup = ({ onClose, onLogin }) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const handleUsernameChange = (event) => {
    setUsername(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleLogin = async () => {
    const values = {
      username,
      password,
    };

    try {
      const response = await fetch(`${import.meta.env.VITE_API}login`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(values),
      });

      if (response.ok) {
        const loggedIn = await response.json();
        console.log(loggedIn);
        if (loggedIn) {
          if (onLogin) {
            onLogin({ username, password });
          }
          onClose();
        }
        console.log(loggedIn.cart);
      } else {
        console.error("Login failed:", response.statusText);
      }
    } catch (error) {
      console.error("Error during login:", error.message);
    }
  };

  return (
    <div className="login-popup">
      <div className="login-popup-content">
        <h2>Login to Continue</h2>
        <label htmlFor="username">Username:</label>
        <input
          type="text"
          id="username"
          name="username"
          value={username}
          onChange={handleUsernameChange}
          required
        />

        <label htmlFor="password">Password:</label>
        <input
          type="password"
          id="password"
          name="password"
          value={password}
          onChange={handlePasswordChange}
          required
        />

        <button onClick={handleLogin}>Login</button>
        <button onClick={onClose}>Cancel</button>
      </div>
    </div>
  );
};

export default LoginPopup;
