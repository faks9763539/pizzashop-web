import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  setGuest,
  setLogin,
  setStayLoggedIn,
} from "../../features/customerSlice";
import "./Auth.css";

export const Auth = ({ onClose }) => {
  const [values, setValues] = useState({
    username: "",
    password: "",
    address: "",
    firstName: "",
    lastName: "",
  });

  const [pageType, setPageType] = useState("login");
  const dispatch = useDispatch();
  const isLogin = pageType === "login";
  const isRegister = pageType === "register";
  const isGuest = useSelector((state) => state.guest === true);
  const stayLoggedIn = useSelector((state) => state.stayLoggedIn);
  const [loginError, setLoginError] = useState(false);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setValues((prevValues) => ({ ...prevValues, [name]: value }));
  };

  const handleStayLoggedInChange = () => {
    dispatch(setStayLoggedIn(!stayLoggedIn));
  };

  const inputFields = [
    { label: "Username", name: "username", type: "text" },
    { label: "Password", name: "password", type: "password" },
    { label: "Address", name: "address", type: "text", showOnRegister: true },
    {
      label: "First Name",
      name: "firstName",
      type: "text",
      showOnRegister: true,
    },
    {
      label: "Last Name",
      name: "lastName",
      type: "text",
      showOnRegister: true,
    },
  ];

  const register = async (values) => {
    try {
      const response = await fetch(`${import.meta.env.VITE_API}register`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(values),
      });

      if (response.ok) {
        const savedUser = await response.json();
        setPageType("login");
      } else {
        console.error("Registration failed:", response.statusText);
      }
    } catch (error) {
      console.error("Error during registration:", error.message);
    }
  };

  const login = async (values) => {
    try {
      const response = await fetch(`${import.meta.env.VITE_API}login`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(values),
      });

      if (response.ok) {
        const loggedIn = await response.json();
        console.log(loggedIn);
        if (loggedIn) {
          dispatch(
            setLogin({
              customer: loggedIn,
              token: loggedIn.token,
              cart: loggedIn.cart,
              guest: false,
            })
          );
          onClose();
        }
        setLoginError(false);
      } else {
        console.error("Login failed:", response.statusText);
        setLoginError(true);
      }
    } catch (error) {
      console.error("Error during login:", error.message);
      setLoginError(true);
    }
  };

  const handleFormSubmit = async (e) => {
    e.preventDefault();

    if (isLogin) {
      await login(values);
    }

    if (isRegister) {
      await register(values);
    }
  };

  const togglePageType = () => {
    setPageType(isLogin ? "register" : "login");
  };

  const handleGuest = () => {
    dispatch(setGuest({ guest: true }));
    console.log("guest is", isGuest);

    onClose();
  };

  return (
    <div className="auth-overlay">
      <div className="overlay-content">
        <button className="x-btn" onClick={onClose}>
          X
        </button>
        <div className="auth-container">
          <form className="auth-form" onSubmit={handleFormSubmit}>
            {inputFields.map(
              (field) =>
                (isRegister || !field.showOnRegister) && (
                  <div key={field.name}>
                    <label htmlFor={field.name}>{field.label}:</label>
                    <input
                      type={field.type}
                      id={field.name}
                      name={field.name}
                      value={values[field.name]}
                      onChange={handleInputChange}
                      required
                    />
                  </div>
                )
            )}
            {isLogin && loginError && (
              <div>Incorrect username or password.</div>
            )}
            <button type="submit">{isLogin ? "Login" : "Register"}</button>
          </form>

          <label>
            Stay Logged In:
            <input className="checkbox"
              type="checkbox"
              checked={stayLoggedIn || false}
              onChange={handleStayLoggedInChange}
            />
          </label>
          <p>
            {isLogin ? "New here? " : "Already have an account? "}
            <button onClick={togglePageType}>
              {isLogin ? "Register!" : "Login"}
            </button>
          </p>

          <p>
            <button onClick={handleGuest}>Continue as Guest</button>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Auth;
