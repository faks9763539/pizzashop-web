import Customer from "../models/customer.js";

/* POST*/
export const Checkout = async (req, res) => {
  try {
    // const { firstName, lastName, address, paymentMethod, cardInfo, items } =
    //   req.body;

    /*
    Ovdje bi precesirali podatke korisnika, naplatu kartice i sve ostalo
    */

    const generateOrderID = () => {
      const characters =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      const length = 10;
      let orderId = "";

      for (let i = 0; i < length; i++) {
        const randomIndex = Math.floor(Math.random() * characters.length);
        orderId += characters.charAt(randomIndex);
      }

      return orderId;
    };

    const orderId = generateOrderID();

    res.status(200).json({ success: true, orderId });
  } catch (err) {
    console.error("Error creating product:", err);
    res.status(409).json({ success: false, message: err.message });
  }
};

/* GET */
export const GetCheckoutInfo = async (req, res) => {
  try {
    const { customerId } = req.params;
    const customer = await Customer.findById(customerId);

    const customerInfo = new Customer({
      firstName: customer.firstName,
      lastName: customer.lastName,
      address: customer.address,
      paymentMethod: customer.paymentMethod,
      cardInfo: customer.cardInfo,
    });
    res.status(200).json(customerInfo);
  } catch (err) {
    res.status(404).json({ message: err.message });
  }
};
