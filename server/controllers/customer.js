import bcrypt from "bcrypt";
import Customer from "../models/customer.js";

/* GET */

export const getCustomer = async (req, res) => {
  try {
    const { id } = req.params;
    const customer = await Customer.findById(id);
    if (!customer) {
      return res.status(404).json({ message: "Customer not found" });
    }
    const customerWithoutPassword = customer.toObject();
    delete customerWithoutPassword.password;
    res.json(customerWithoutPassword);
  } catch (err) {
    res.status(404).json({ message: err.message });
  }
};

/* PATCH */
export const updateCustomer = async (req, res) => {
  try {
    const { id } = req.params;
    const {
      firstName,
      lastName,
      username,
      password,
      address,
      paymentMethod,
      cardInfo,
      newPassword,
    } = req.body;

    console.log(req.body);

    const customer = await Customer.findById(id);
    if (!customer)
      return res
        .status(400)
        .json({ msg: "Customer does not exist.", success: false });

    console.log(customer.password);
    const isMatch = await bcrypt.compare(password, customer.password);
    if (!isMatch) {
      return res
        .status(400)
        .json({ msg: "Invalid username or password.", success: false });
    }
    const salt = await bcrypt.genSalt();
    const passwordHash = await bcrypt.hash(
      newPassword && newPassword.trim() !== "" ? newPassword : password,
      salt
    );

    const updatedCustomer = await Customer.findByIdAndUpdate(
      id,
      {
        firstName,
        lastName,
        username,
        password: passwordHash,
        address,
        paymentMethod,
        cardInfo,
      },
      { new: true }
    );
    const customerWithoutPassword = updatedCustomer.toObject();
    delete customerWithoutPassword.password;
    res.status(200).json(customerWithoutPassword);
  } catch (err) {
    res.status(404).json({ message: err.message });
  }
};

/* DELETE */

export const deleteCustomer = async (req, res) => {
  try {
    const { id } = req.params;
    const { username, password } = req.body;

    const customer = await Customer.findById(id);
    if (!customer)
      return res
        .status(400)
        .json({ msg: "User does not exist.", success: false });

    const isMatch = bcrypt.compare(password, customer.password);
    if (!isMatch) {
      return res
        .status(400)
        .json({ msg: "Invalid username or password.", success: false });
    }
    await Customer.findByIdAndDelete(id)
      .then((doc) => {
        res.status(200).json(true);
      })
      .catch((err) => {
        res.status(500).json({ error: err.message, success: false });
      });
  } catch (err) {
    res.status(500).json({ error: err.message, success: false });
  }
};
