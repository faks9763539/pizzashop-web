import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import Customer from "../models/customer.js";

/* REGISTERING */

export const register = async (req, res) => {
  try {
    const {
      firstName,
      lastName,
      username,
      password,
      address,
      paymentMethod,
      cardInfo,
    } = req.body;

    const salt = await bcrypt.genSalt();
    const passwordHash = await bcrypt.hash(password, salt);

    const newCustomer = new Customer({
      firstName,
      lastName,
      username,
      password: passwordHash,
      address,
      paymentMethod,
      cardInfo,
    });

    const savedCustomer = await newCustomer.save();
    res.status(201).json({ id: savedCustomer.id });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

/* LOGGING IN */

export const login = async (req, res) => {
  try {
    const { username, password } = req.body;
    const customer = await Customer.findOne({ username: username });
    if (!customer) return res.status(400).json({ msg: "User does not exist." });

    const isMatch = await bcrypt.compare(password, customer.password);
    if (!isMatch)
      return res.status(400).json({ msg: "Invalid username or password." });
    const token = jwt.sign({ id: customer._id }, process.env.JWT_SECRET);
    delete customer.password;
    res.status(200).json({
      token,
      id: customer.id,
      firstName: customer.firstName,
      lastName: customer.lastName,
      username: customer.username,
      address: customer.address,
      isAdmin: customer.isAdmin,
      cart: customer.cart,
    });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};
