let lastOrderId = null;
let orderStatusIndex = 0;

/* GET */
export const GetOrderStatus = async (req, res) => {
  try {
    const { orderId } = req.params;

    const orderStatuses = [
      "Order confirmed",
      "Order being prepared",
      "Order ready",
      "Order out for delivery",
      "Order delivered",
    ];

    if (orderId !== lastOrderId) {
      orderStatusIndex = 0;
      lastOrderId = orderId;
    }

    const statusMessage =
      orderStatuses[orderStatusIndex % orderStatuses.length];

    orderStatusIndex = (orderStatusIndex + 1) % orderStatuses.length;

    res.status(200).json({ statusMessage });
  } catch (err) {
    res.status(404).json({ message: err.message });
  }
};
