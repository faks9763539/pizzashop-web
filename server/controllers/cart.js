import Customer from "../models/customer.js";

/* POST & PATCH*/
export const createUpdateCart = async (req, res) => {
  try {
    const { items } = req.body;
    const { customerId } = req.params;
    if (!customerId || !items || !Array.isArray(items)) {
      return res.status(400).json({ error: "Invalid request format" });
    }

    const mappedItems = items.map(({ _id, name, size, amount }) => ({
      name: name,
      size: size,
      amount: amount,
      _id: _id,
    }));
    console.log(items);
    const updatedCustomer = await Customer.findByIdAndUpdate(
      customerId,
      { $set: { cart: mappedItems } },
      { new: true }
    );

    const success = updatedCustomer !== null;

    res.status(200).json({ success });
  } catch (err) {
    console.error("Error creating product:", err);
    res.status(409).json({ success: false, message: err.message });
  }
};

/* GET */
export const getCart = async (req, res) => {
  try {
    const { customerId } = req.params;
    const customer = await Customer.findById(customerId);
    const itemsInCart = customer.cart;

    res.status(200).json(itemsInCart);
  } catch (err) {
    res.status(404).json({ message: err.message });
  }
};

/* DELETE */

export const deleteCart = async (res, req) => {
  try {
    const { customerId } = req.params;

    const updatedCustomer = await Customer.findByIdAndUpdate(
      customerId,
      { $set: { cart: [] } },
      { new: true }
    );

    if (!updatedCustomer) {
      return res.status(404).json({ success: false });
    }

    res.status(200).json({
      success: true,
    });
  } catch (err) {
    res.status(404).json({ message: err.message });
  }
};
