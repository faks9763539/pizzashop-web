import Ingredient from "../models/ingredient.js";
import Customer from "../models/customer.js";

/* POST */
export const createIngredient = async (req, res) => {
  try {
    const { name, quantity, unit, customerId } = req.body;
    const customer = await Customer.findById(customerId);
    if (customer.isAdmin === true) {
      const newIngredient = new Ingredient({
        name,
        quantity,
        unit,
      });

      await newIngredient.save();
      const ingredient = await Ingredient.find();
      res.status(201).json(ingredient);
    } else {
      throw new Error("authError");
    }
  } catch (err) {
    console.error("Error creating ingredient:", err);
    res.status(409).json({ message: err.message });
  }
};

/* GET */
export const getIngredients = async (req, res) => {
  try {
    const { customerId } = req.params;

    const customer = await Customer.findById(customerId);
    if (customer.isAdmin === true) {
      const ingredients = await Ingredient.find();
      res.status(200).json(ingredients);
    } else {
      throw new Error("authError");
    }
  } catch (err) {
    res.status(404).json({ message: err.message });
  }
};

/* PATCH */
export const updateIngredient = async (req, res) => {
  try {
    const { ingredientId } = req.params;
    const { name, quantity, unit, customerId } = req.body;
    console.log(ingredientId, name, quantity, unit, customerId);
    const customer = await Customer.findById(customerId);
    if (customer.isAdmin === true) {
      const updatedIngredient = await Ingredient.findByIdAndUpdate(
        ingredientId,
        { name, quantity, unit },
        { new: true }
      );
      res.status(200).json(updatedIngredient);
    } else {
      throw new Error("authError");
    }
  } catch (err) {
    res.status(404).json({ message: err.message });
  }
};

/* DELETE */
export const deleteIngredient = async (req, res) => {
  try {
    const { ingredientId } = req.params;
    const { customerId } = req.body;
    const customer = await Customer.findById(customerId);

    if (customer.isAdmin === true) {
      await Ingredient.findByIdAndDelete(ingredientId);
      res.status(200).json(true);
    } else {
      throw new Error("authError");
    }
  } catch (err) {
    res.status(401).json({ message: err.message });
  }
};
