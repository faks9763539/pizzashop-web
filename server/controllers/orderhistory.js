import Customer from "../models/customer.js";

/* POST*/
export const SetOrderHistory = async (req, res) => {
  try {
    const { customerId, items } = req.body;
    const { orderId } = req.params;
    const newOrder = {
      orderId,
      items,
    };

    const updatedCustomer = await Customer.findByIdAndUpdate(
      customerId,
      { $push: { orderHistory: newOrder } },
      { new: true }
    );

    if (!updatedCustomer) {
      return res
        .status(404)
        .json({ success: false, message: "Customer not found" });
    }

    res.status(200).json({ success: true });
  } catch (err) {
    console.error("Error creating product:", err);
    res.status(409).json({ success: false, message: err.message });
  }
};

/* GET */
export const GetOrderHistory = async (req, res) => {
  try {
    const { customerId } = req.params;
    const customer = await Customer.findById(customerId);

    res.status(200).json(customer.orderHistory);
  } catch (err) {
    res.status(404).json({ message: err.message });
  }
};
