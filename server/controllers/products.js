import Product from "../models/products.js";
import Customer from "../models/customer.js";
import Ingredient from "../models/ingredient.js";

/* POST */
export const createProduct = async (req, res) => {
  try {
    const { name, price, size, imageUrl, ingredients, customerId } = req.body;

    const customer = await Customer.findById(customerId);
    if (customer.isAdmin === true) {
      const newProduct = new Product({
        name,
        price,
        size,
        imageUrl,
        ingredients,
      });

      await newProduct.save();
      const products = await Product.find();
      res.status(201).json(products);
    } else {
      throw new Error("authError");
    }
  } catch (err) {
    console.error("Error creating product:", err);
    res.status(409).json({ message: err.message });
  }
};

/* GET */
export const getProducts = async (req, res) => {
  try {
    // Find all products
    const products = await Product.find();

    const filteredProducts = [];

    for (const product of products) {
      // Assuming there's an ingredients field in Product model
      const ingredients = product.ingredients || [];

      // Check each ingredient
      let anyIngredientHasZeroQuantity = false;

      for (const ingredient of ingredients) {
        // Find the ingredient by name
        const foundIngredient = await Ingredient.findOne({
          name: ingredient,
        });
        // Check if the ingredient exists and has a non-zero quantity
        if (!foundIngredient || foundIngredient.quantity <= 0) {
          anyIngredientHasZeroQuantity = true;
          break;
        }
      }

      // If any ingredient has a zero quantity, return the product without sizes
      if (anyIngredientHasZeroQuantity) {
        filteredProducts.push({ ...product.toObject(), size: [] });
      } else {
        // If all ingredients have non-zero quantities, include the product in the result
        filteredProducts.push(product);
      }
    }

    res.status(200).json(filteredProducts);
  } catch (err) {
    res.status(404).json({ message: err.message });
  }
};

/* PATCH */
export const updateProduct = async (req, res) => {
  try {
    const { productId } = req.params;
    const { name, price, size, imageUrl, ingredients, customerId } = req.body;
    const customer = await Customer.findById(customerId);
    if (customer.isAdmin === true) {
      await Product.findByIdAndUpdate(
        productId,
        { name, price, size, imageUrl, ingredients },
        { new: true }
      );
      const products = await Product.find();
      res.status(200).json(products);
    } else {
      throw new Error("authError");
    }
  } catch (err) {
    res.status(404).json({ message: err.message });
  }
};

/* DELETE */
export const deleteProduct = async (req, res) => {
  try {
    const { productId } = req.params;
    const { customerId } = req.body;
    const customer = await Customer.findById(customerId);

    if (customer.isAdmin === true) {
      await Product.findByIdAndDelete(productId);
      res.status(200).json(true);
    } else {
      throw new Error("authError");
    }
  } catch (err) {
    res.status(401).json({ message: err.message });
  }
};
