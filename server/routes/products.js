import express from "express";
import {
  createProduct,
  getProducts,
  updateProduct,
  deleteProduct,
} from "../controllers/products.js";
import { verifyToken } from "../middleware/auth.js";

const router = express.Router();

router.post("/", verifyToken, createProduct);
router.get("/", getProducts);
router.patch("/:productId", verifyToken, updateProduct);
router.delete("/:productId", verifyToken, deleteProduct);

export default router;
