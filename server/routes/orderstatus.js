import express from "express";
import { GetOrderStatus } from "../controllers/orderstatus.js";

const router = express.Router();

router.get("/:orderId", GetOrderStatus);

export default router;
