import express from "express";

import {
  getCustomer,
  updateCustomer,
  deleteCustomer,
} from "../controllers/customer.js";
import { verifyToken } from "../middleware/auth.js";

const router = express.Router();

router.get("/:id", verifyToken, getCustomer);
router.patch("/:id", verifyToken, updateCustomer);
router.delete("/:id", verifyToken, deleteCustomer);

export default router;
