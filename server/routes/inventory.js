import express from "express";
import {
  getIngredients,
  createIngredient,
  updateIngredient,
  deleteIngredient,
} from "../controllers/inventory.js";
import { verifyToken } from "../middleware/auth.js";

const router = express.Router();

router.post("/", verifyToken, createIngredient);
router.get("/:customerId", verifyToken, getIngredients);
router.patch("/:ingredientId", verifyToken, updateIngredient);
router.delete("/:ingredientId", verifyToken, deleteIngredient);

export default router;
