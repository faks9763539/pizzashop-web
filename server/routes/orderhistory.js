import express from "express";
import {
  SetOrderHistory,
  GetOrderHistory,
} from "../controllers/orderhistory.js";
import { verifyToken } from "../middleware/auth.js";

const router = express.Router();

router.post("/:orderId", verifyToken, SetOrderHistory);
router.get("/:customerId", verifyToken, GetOrderHistory);

export default router;
