import express from "express";
import { Checkout, GetCheckoutInfo } from "../controllers/checkout.js";
import { verifyToken } from "../middleware/auth.js";
const router = express.Router();

router.post("/", Checkout);
router.get("/:customerId", verifyToken, GetCheckoutInfo);

export default router;
