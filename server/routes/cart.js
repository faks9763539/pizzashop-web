import express from "express";
import { createUpdateCart, getCart, deleteCart } from "../controllers/cart.js";
import { verifyToken } from "../middleware/auth.js";

const router = express.Router();

router.post("/:customerId", verifyToken, createUpdateCart);
router.get("/:customerId", verifyToken, getCart);
router.patch("/:customerId", verifyToken, createUpdateCart);
router.delete("/:customerId", verifyToken, deleteCart);

export default router;