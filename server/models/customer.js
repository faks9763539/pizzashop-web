import mongoose from "mongoose";

const CustomerSchema = new mongoose.Schema(
  {
    firstName: { type: String, required: true, min: 1, max: 50 },
    lastName: { type: String, required: true, min: 1, max: 50 },
    username: { type: String, required: true, max: 50, unique: true },
    password: { type: String, required: true, min: 5 },
    address: { type: String, required: true, min: 1, max: 100 },
    paymentMethod: { type: String, default: "Cash" },
    cardInfo: {
      type: {
        cardType: { type: String },
        nameOnCard: { type: String },
        cardNumber: { type: String },
        cardExpirationDate: { type: String },
      },
      default: {},
    },
    cart: {
      type: [
        {
          name: { type: String },
          size: { type: String },
          amount: { type: Number },
        },
      ],
      default: [],
    },
    orderHistory: {
      type: [
        {
          orderId: { type: String, required: true },
          items: [
            {
              name: { type: String, required: true },
              size: { type: String, required: true },
              amount: { type: Number, required: true },
            },
          ],
        },
      ],
      default: [],
    },
    isAdmin: { type: Boolean, default: false },
  },
  { timestamps: true }
);

const Customer = mongoose.model("Customer", CustomerSchema);
export default Customer;
