import mongoose from "mongoose";
const { Schema } = mongoose;

const ProductSchema = new Schema({
  name: { type: String, required: true, min: 2, max: 50 },
  price: { type: Number, required: true, min: 1 },
  size: { type: [String], required: true, min: 1, max: 50 },
  imageUrl: { type: String, default: "" },
  ingredients: { type: [String], required: true },
});

const Product = mongoose.model("Products", ProductSchema);
export default Product;
