import productsRouter from "../routes/products.js";
import customerRoutes from "../routes/customer.js";
import registerRoutes from "../routes/register.js";
import loginRoutes from "../routes/login.js";
import cartRoutes from "../routes/cart.js";
import checkoutRoutes from "../routes/checkout.js";
import orderHistoryRoutes from "../routes/orderhistory.js";
import orderStatusRoutes from "../routes/orderstatus.js";
import inventoryRoutes from "../routes/inventory.js";
import mongoose from "mongoose";
import express from "express";
import cors from "cors";

const app = express();
app.use(cors());

const PORT = 3000;

app.use(express.json({ limit: "30mb" }));

app.use("/register", registerRoutes);
app.use("/login", loginRoutes);
app.use("/customer", customerRoutes);
app.use("/products", productsRouter);
app.use("/cart", cartRoutes);
app.use("/checkout", checkoutRoutes);
app.use("/orderstatus", orderStatusRoutes);
app.use("/orderhistory", orderHistoryRoutes);
app.use("/inventory", inventoryRoutes);

mongoose
  .connect(process.env.MONGO_URL)
  .then(() => {
    app.listen(PORT, () => console.log(`Server started. Server port: ${PORT}`));
  })
  .catch((error) => console.error(`${error}, did not connect.`));
